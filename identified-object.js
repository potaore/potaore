var UTIL = require('./utility');
var IdentifiedObject = (function () {
    function IdentifiedObject(name) {
        this.id = UTIL.Utility.uuid();
        this.name = name;
    }
    IdentifiedObject.prototype.equals = function (another) {
        return this.id === another.id;
    };
    IdentifiedObject.prototype.notEquals = function (another) {
        return this.id !== another.id;
    };
    return IdentifiedObject;
})();
exports.IdentifiedObject = IdentifiedObject;
var ReversibleMap = (function () {
    function ReversibleMap() {
        this.map = {};
        this.reverseMap = {};
    }
    ReversibleMap.prototype.put = function (key, value) {
        this.map[key] = value;
        if (!this.reverseMap.hasOwnProperty(value)) {
            this.reverseMap[value] = {};
        }
        this.reverseMap[value][key] = value;
    };
    ReversibleMap.prototype.remove = function (key) {
        var value = this.map[key];
        delete this.reverseMap[value][key];
        delete this.map[key];
    };
    ReversibleMap.prototype.getReverse = function (value) {
        if (!this.reverseMap.hasOwnProperty(value)) {
            this.reverseMap[value] = {};
        }
        return this.reverseMap[value];
    };
    return ReversibleMap;
})();
exports.ReversibleMap = ReversibleMap;
var IdentifiedObjectContainer = (function () {
    function IdentifiedObjectContainer() {
        this.glovalMap = {};
        this.groupMap = new ReversibleMap();
        this.tagMap = {};
    }
    IdentifiedObjectContainer.prototype.put = function (object, group) {
        if (group === void 0) { group = ''; }
        this.glovalMap[object.id] = object;
        this.groupMap.put(object.id, group);
    };
    IdentifiedObjectContainer.prototype.get = function (id) { return this.glovalMap[id]; };
    IdentifiedObjectContainer.prototype.changeGroup = function (object, group) {
        if (group === void 0) { group = ''; }
        this.groupMap.remove(object.id);
        this.groupMap.put(object.id, group);
    };
    IdentifiedObjectContainer.prototype.remove = function (object) {
        this.groupMap.remove(object.id);
        delete this.glovalMap[object.id];
    };
    IdentifiedObjectContainer.prototype.getGroup = function (group) {
        return this.groupMap.getReverse(group);
    };
    return IdentifiedObjectContainer;
})();
exports.IdentifiedObjectContainer = IdentifiedObjectContainer;
