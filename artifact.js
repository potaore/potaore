var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var UTIL = require('./utility');
var IDOBJ = require('./identified-object');
var Artifact = (function (_super) {
    __extends(Artifact, _super);
    function Artifact(name) {
        _super.call(this, name);
        this._connectedArtifacts = {};
        this._listnerArtifacts = {};
        this._broadcasterArtifacts = {};
        this._events = {};
        this._loopEvents = {};
    }
    Artifact.prototype.connect = function (anotherArtifacts) {
        anotherArtifacts._connectedArtifacts[this.id] = this;
        this._connectedArtifacts[anotherArtifacts.id] = anotherArtifacts;
        return this;
    };
    Artifact.prototype.listen = function (anotherArtifacts) {
        anotherArtifacts._listnerArtifacts[this.id] = this;
        this._broadcasterArtifacts[anotherArtifacts.id] = anotherArtifacts;
        return this;
    };
    Artifact.prototype.disconnect = function (anotherArtifacts) {
        delete anotherArtifacts._connectedArtifacts[this.id];
        delete this._connectedArtifacts[anotherArtifacts.id];
        delete anotherArtifacts._listnerArtifacts[this.id];
        delete this._broadcasterArtifacts[anotherArtifacts.id];
        return this;
    };
    Artifact.prototype.disconnectAll = function () {
        for (var anotherCellId in this._connectedArtifacts) {
            this.disconnect(this._connectedArtifacts[anotherCellId]);
        }
        return this;
    };
    Artifact.prototype.synchronizeConnectionInfo = function (anotherArtifact) {
        for (var key in anotherArtifact._listnerArtifacts) {
            anotherArtifact._listnerArtifacts[key].listen(this);
        }
        for (var key in anotherArtifact._broadcasterArtifacts) {
            this.listen(anotherArtifact._broadcasterArtifacts[key]);
        }
        for (var key in anotherArtifact._connectedArtifacts) {
            this.connect(anotherArtifact._connectedArtifacts[key]);
        }
        return this;
    };
    Artifact.prototype.on = function (eventName, eventImpl) {
        this._events[eventName] = eventImpl;
        return this;
    };
    Artifact.prototype.fire = function (eventName, sender, options, callback) {
        var _this = this;
        if (callback === void 0) { callback = Artifact.voidCallback; }
        if (this.localPointcut)
            this.localPointcut.fire(eventName, sender, options);
        var conversedEventName = this.convertEventName(eventName);
        if (this._events.hasOwnProperty(conversedEventName)) {
            this._events[conversedEventName](eventName, sender, options, function (result) { return callback({ artifact: _this, options: result }); });
        }
    };
    Artifact.prototype.notify = function (eventName, options, callback) {
        if (options === void 0) { options = {}; }
        if (callback === void 0) { callback = Artifact.voidCallback; }
        this.fireAllWithoutSender(eventName, this, options, callback);
    };
    Artifact.prototype.fireAllWithoutSender = function (eventName, sender, options, callback) {
        if (callback === void 0) { callback = Artifact.voidCallback; }
        for (var id in this._connectedArtifacts) {
            var cell = this._connectedArtifacts[id];
            if (cell.notEquals(sender)) {
                var result = cell.fire(eventName, sender, options, callback);
            }
        }
        for (var id in this._listnerArtifacts) {
            var cell = this._listnerArtifacts[id];
            if (cell.notEquals(sender)) {
                var result = cell.fire(eventName, sender, options, callback);
            }
        }
    };
    Artifact.prototype.convertEventName = function (eventName) {
        return eventName;
    };
    Artifact.prototype.startRepeat = function (eventName, impl, interval) {
        if (this._loopEvents.hasOwnProperty(eventName)) {
            return;
        }
        this._loopEvents[eventName] = new Repeater(eventName, interval, impl);
        this._loopEvents[eventName].start();
    };
    Artifact.prototype.stopRepeat = function (eventName) {
        if (this._loopEvents.hasOwnProperty(eventName)) {
            return;
        }
        this._loopEvents[eventName].stop();
        delete this._loopEvents[eventName];
    };
    Artifact.prototype.destroy = function () {
        for (var id in this._connectedArtifacts) {
            this.disconnect(this._connectedArtifacts[id]);
        }
        delete this._events;
    };
    Artifact.voidCallback = function () { };
    return Artifact;
})(IDOBJ.IdentifiedObject);
exports.Artifact = Artifact;
var Listner = (function (_super) {
    __extends(Listner, _super);
    function Listner(logger, showdata) {
        if (showdata === void 0) { showdata = false; }
        _super.call(this, 'listner');
        this.localPointcut = {
            fire: function (eventName, sender, options) {
                var cache = [];
                if (showdata) {
                    logger.info('"' + sender.name + '"' + '::' + '"' + eventName + '"' + '::' + JSON.stringify(options, function (key, value) {
                        if ((typeof value == 'object') && (value != null)) {
                            if (-1 < cache.indexOf(value))
                                return;
                            cache.push(value);
                        }
                        return value;
                    }));
                }
                else {
                    logger.info('"' + sender.name + '"' + '::' + '"' + eventName + '"');
                }
            }
        };
    }
    return Listner;
})(Artifact);
exports.Listner = Listner;
var Repeater = (function (_super) {
    __extends(Repeater, _super);
    function Repeater(name, interval, impl) {
        _super.call(this, name);
        this.running = false;
        this.impl = impl;
        this.interval = interval;
    }
    Repeater.prototype.start = function () {
        var _this = this;
        if (this.running)
            return;
        this.running = true;
        var loop = function () {
            if (!_this.running)
                return;
            var startTime = UTIL.Utility.getTime();
            _this.impl();
            var endTime = UTIL.Utility.getTime();
            var delay = _this.interval - endTime + startTime;
            console.log('delay : ' + delay);
            setTimeout(loop, delay < 0 ? 0 : delay);
        };
        loop();
    };
    Repeater.prototype.stop = function () {
        this.running = false;
    };
    return Repeater;
})(IDOBJ.IdentifiedObject);
exports.Repeater = Repeater;
var ScenarioPublisher = (function (_super) {
    __extends(ScenarioPublisher, _super);
    function ScenarioPublisher(name) {
        _super.call(this, name);
        this.scenarios = {};
    }
    ScenarioPublisher.prototype.createScenario = function (scenarioName, buildAction) {
        var _this = this;
        var startEvent = new ScenarioEventStart();
        buildAction(new EventChainBuilder(startEvent));
        return this.scenarios[scenarioName] = function (options) { return startEvent.execute({ artifact: _this, options: options }, function () { }); };
    };
    ScenarioPublisher.prototype.executeScenario = function (scenarioName, options) {
        this.scenarios[scenarioName](options);
    };
    return ScenarioPublisher;
})(Artifact);
exports.ScenarioPublisher = ScenarioPublisher;
var ScenarioEvent = (function () {
    function ScenarioEvent() {
        this.nexts = [];
    }
    ScenarioEvent.prototype.setNext = function (next) {
        this.nexts.push(next);
    };
    ScenarioEvent.prototype.execute = function (eventContext, callback) {
        var _this = this;
        this.executeImpl(eventContext, function (result) {
            var i = 0;
            while (i < _this.nexts.length) {
                _this.nexts[i].execute(result, callback);
                i = (i + 1) | 0;
            }
        });
    };
    return ScenarioEvent;
})();
exports.ScenarioEvent = ScenarioEvent;
var ScenarioEventStart = (function (_super) {
    __extends(ScenarioEventStart, _super);
    function ScenarioEventStart() {
        _super.call(this);
    }
    ScenarioEventStart.prototype.executeImpl = function (eventContext, callback) {
        callback(eventContext);
    };
    return ScenarioEventStart;
})(ScenarioEvent);
exports.ScenarioEventStart = ScenarioEventStart;
var ScenarioEventFire = (function (_super) {
    __extends(ScenarioEventFire, _super);
    function ScenarioEventFire(eventName) {
        _super.call(this);
        this.eventName = eventName;
    }
    ScenarioEventFire.prototype.executeImpl = function (eventContext, callback) {
        eventContext.artifact.fire(this.eventName, eventContext.artifact, eventContext.options, callback);
    };
    return ScenarioEventFire;
})(ScenarioEvent);
exports.ScenarioEventFire = ScenarioEventFire;
var ScenarioEventNotify = (function (_super) {
    __extends(ScenarioEventNotify, _super);
    function ScenarioEventNotify(eventName) {
        _super.call(this);
        this.eventName = eventName;
    }
    ScenarioEventNotify.prototype.executeImpl = function (eventContext, callback) {
        eventContext.artifact.notify(this.eventName, eventContext.options, callback);
    };
    return ScenarioEventNotify;
})(ScenarioEvent);
exports.ScenarioEventNotify = ScenarioEventNotify;
var ScenarioEventWhere = (function (_super) {
    __extends(ScenarioEventWhere, _super);
    function ScenarioEventWhere(guard) {
        _super.call(this);
        this.guard = guard;
    }
    ScenarioEventWhere.prototype.executeImpl = function (eventContext, callback) {
        if (this.guard(eventContext.options)) {
            callback(eventContext);
        }
    };
    return ScenarioEventWhere;
})(ScenarioEvent);
exports.ScenarioEventWhere = ScenarioEventWhere;
var ScenarioEventPointcut = (function (_super) {
    __extends(ScenarioEventPointcut, _super);
    function ScenarioEventPointcut(pointcut) {
        _super.call(this);
        this.pointcut = pointcut;
    }
    ScenarioEventPointcut.prototype.executeImpl = function (eventContext, callback) {
        var newEventContext = this.pointcut(eventContext);
        callback(newEventContext);
    };
    return ScenarioEventPointcut;
})(ScenarioEvent);
exports.ScenarioEventPointcut = ScenarioEventPointcut;
var EventChainBuilder = (function () {
    function EventChainBuilder(prevEvent) {
        this.prevEvent = prevEvent;
    }
    EventChainBuilder.prototype.fire = function (eventName) {
        var ev = new ScenarioEventFire(eventName);
        this.prevEvent.setNext(ev);
        return new EventChainBuilder(ev);
    };
    EventChainBuilder.prototype.notify = function (eventName) {
        var ev = new ScenarioEventNotify(eventName);
        this.prevEvent.setNext(ev);
        return new EventChainBuilder(ev);
    };
    EventChainBuilder.prototype.where = function (guard) {
        var ev = new ScenarioEventWhere(guard);
        this.prevEvent.setNext(ev);
        return new EventChainBuilder(ev);
    };
    EventChainBuilder.prototype.pointcut = function (pointcut) {
        var ev = new ScenarioEventPointcut(pointcut);
        this.prevEvent.setNext(ev);
        return new EventChainBuilder(ev);
    };
    return EventChainBuilder;
})();
exports.EventChainBuilder = EventChainBuilder;
