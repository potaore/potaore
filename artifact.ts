import UTIL = require('./utility');
import IDOBJ = require('./identified-object');

export class Artifact extends IDOBJ.IdentifiedObject {
  static voidCallback = () => { };
  public localPointcut: any;
  protected _connectedArtifacts: { [code: string]: Artifact; };
  protected _listnerArtifacts: { [code: string]: Artifact; };
  protected _broadcasterArtifacts: { [code: string]: Artifact; };
  protected _events: any;
  protected _loopEvents: { [code: string]: Repeater; };
  constructor(name: string) {
    super(name);
    this._connectedArtifacts = {};
    this._listnerArtifacts = {};
    this._broadcasterArtifacts = {};
    this._events = {};
    this._loopEvents = {};
  }
  connect(anotherArtifacts: Artifact) {
    anotherArtifacts._connectedArtifacts[this.id] = this;
    this._connectedArtifacts[anotherArtifacts.id] = anotherArtifacts;
    return this;
  }
  listen(anotherArtifacts: Artifact) {
    anotherArtifacts._listnerArtifacts[this.id] = this;
    this._broadcasterArtifacts[anotherArtifacts.id] = anotherArtifacts;
    return this;
  }
  disconnect(anotherArtifacts: Artifact) {
    delete anotherArtifacts._connectedArtifacts[this.id];
    delete this._connectedArtifacts[anotherArtifacts.id];
    delete anotherArtifacts._listnerArtifacts[this.id];
    delete this._broadcasterArtifacts[anotherArtifacts.id];
    return this;
  }
  disconnectAll() {
    for (let anotherCellId in this._connectedArtifacts) {
      this.disconnect(this._connectedArtifacts[anotherCellId]);
    }
    return this;
  }
  synchronizeConnectionInfo(anotherArtifact : Artifact) {
    for(let key in anotherArtifact._listnerArtifacts) {
      anotherArtifact._listnerArtifacts[key].listen(this);
    }
    for(let key in anotherArtifact._broadcasterArtifacts) {
      this.listen(anotherArtifact._broadcasterArtifacts[key]);
    }
    for(let key in anotherArtifact._connectedArtifacts) {
      this.connect(anotherArtifact._connectedArtifacts[key]);
    }
    return this;
  }
  
  on(eventName: string, eventImpl: { (eventName: string, sender: Artifact, options: any, callback: Function): void; }) {
    this._events[eventName] = eventImpl;
    return this;
  }
  fire(eventName: string, sender: Artifact, options: any, callback: Function = Artifact.voidCallback) {
    if (this.localPointcut) this.localPointcut.fire(eventName, sender, options);
    let conversedEventName = this.convertEventName(eventName);
    if (this._events.hasOwnProperty(conversedEventName)) {
      this._events[conversedEventName](eventName, sender, options, (result) => callback({ artifact: this, options: result }));
    }
  }
  notify(eventName: string, options: any = {}, callback: Function = Artifact.voidCallback) {
    this.fireAllWithoutSender(eventName, this, options, callback);
  }
  protected fireAllWithoutSender(eventName: string, sender: Artifact, options: any, callback: Function = Artifact.voidCallback) {
    for (let id in this._connectedArtifacts) {
      let cell = this._connectedArtifacts[id];
      if (cell.notEquals(sender)) {
        let result = cell.fire(eventName, sender, options, callback);
      }
    }
    for (let id in this._listnerArtifacts) {
      let cell = this._listnerArtifacts[id];
      if (cell.notEquals(sender)) {
        let result = cell.fire(eventName, sender, options, callback);
      }
    }
  }

  protected convertEventName(eventName: string) {
    return eventName;
  }

  startRepeat(eventName: string, impl: any, interval: number) {
    if (this._loopEvents.hasOwnProperty(eventName)) {
      return;
    }
    this._loopEvents[eventName] = new Repeater(eventName, interval, impl);
    this._loopEvents[eventName].start();
  }
  stopRepeat(eventName: string) {
    if (this._loopEvents.hasOwnProperty(eventName)) {
      return;
    }
    this._loopEvents[eventName].stop();
    delete this._loopEvents[eventName];
  }


  destroy() {
    for (let id in this._connectedArtifacts) {
      this.disconnect(this._connectedArtifacts[id]);
    }
    delete this._events;
  }
}

export class Listner extends Artifact {
  constructor(logger, showdata = false) {
    super('listner');
    this.localPointcut = {
      fire: (eventName, sender, options) => {
        let cache = [];
        if (showdata) {
          logger.info('"' + sender.name + '"' + '::' + '"' + eventName + '"' + '::' + JSON.stringify(options, (key, value) => {
            if ((typeof value == 'object') && (value != null)) {
              if (-1 < cache.indexOf(value)) return;
              cache.push(value);
            }
            return value;
          }));
        } else {
          logger.info('"' + sender.name + '"' + '::' + '"' + eventName + '"');
        }
      }
    };
  }
}

export class Repeater extends IDOBJ.IdentifiedObject {
  impl: any;
  interval: any;
  running: boolean = false;
  constructor(name, interval, impl) {
    super(name);
    this.impl = impl;
    this.interval = interval;
  }
  start() {
    if (this.running) return;
    this.running = true;
    var loop = () => {
      if (!this.running) return;
      let startTime = UTIL.Utility.getTime();
      this.impl();
      let endTime = UTIL.Utility.getTime();
      let delay = this.interval - endTime + startTime;
      console.log('delay : ' + delay);
      setTimeout(loop, delay < 0 ? 0 : delay);
    }
    loop();
  }
  stop() {
    this.running = false;
  }
}

export interface EventContext {
  artifact: Artifact;
  options: any;
}

export class ScenarioPublisher extends Artifact {
  scenarios: {};
  constructor(name: string) {
    super(name);
    this.scenarios = {};
  }
  createScenario(scenarioName: string, buildAction: { (builder: EventChainBuilder): any; }) {
    let startEvent = new ScenarioEventStart()
    buildAction(new EventChainBuilder(startEvent));
    return this.scenarios[scenarioName] = (options) => startEvent.execute({ artifact: this, options: options }, () => { });
  }
  executeScenario(scenarioName: string, options) {
    this.scenarios[scenarioName](options);
  }
}

export abstract class ScenarioEvent {
  nexts: ScenarioEvent[];
  constructor() {
    this.nexts = [];
  }
  setNext(next: ScenarioEvent) {
    this.nexts.push(next);
  }
  execute(eventContext: EventContext, callback: Function) {
    this.executeImpl(eventContext, (result) => {
      let i = 0;
      while (i < this.nexts.length) {
        this.nexts[i].execute(result, callback);
        i = (i + 1) | 0;
      }
    });
  }
  abstract executeImpl(eventContext: EventContext, callback: Function)
}

export class ScenarioEventStart extends ScenarioEvent {
  constructor() {
    super();
  }
  executeImpl(eventContext: EventContext, callback) {
    callback(eventContext);
  }
}

export class ScenarioEventFire extends ScenarioEvent {
  eventName: string;
  constructor(eventName: string) {
    super();
    this.eventName = eventName;
  }
  executeImpl(eventContext: EventContext, callback) {
    eventContext.artifact.fire(this.eventName, eventContext.artifact, eventContext.options, callback);
  }
}

export class ScenarioEventNotify extends ScenarioEvent {
  eventName: string;
  constructor(eventName: string) {
    super();
    this.eventName = eventName;
  }
  executeImpl(eventContext: EventContext, callback) {
    eventContext.artifact.notify(this.eventName, eventContext.options, callback);
  }
}

export class ScenarioEventWhere extends ScenarioEvent {
  guard: { (options): boolean; };
  constructor(guard: { (options): boolean; }) {
    super();
    this.guard = guard;
  }
  executeImpl(eventContext: EventContext, callback) {
    if (this.guard(eventContext.options)) {
      callback(eventContext);
    }
  }
}

export class ScenarioEventPointcut extends ScenarioEvent {
  pointcut: { (EventContext): any; };
  constructor(pointcut: { (EventContext): any; }) {
    super();
    this.pointcut = pointcut;
  }
  executeImpl(eventContext: EventContext, callback) {
    let newEventContext = this.pointcut(eventContext);
    callback(newEventContext);
  }
}

export interface IEventchainBuilder {
  fire(eventName: string): IEventchainBuilder;
  notify(eventName: string): IEventchainBuilder;
  where(guard: { (options): boolean; }): IEventchainBuilder;
  pointcut(pointcut: { (EventContext): any; }): IEventchainBuilder;
}

export class EventChainBuilder implements IEventchainBuilder {
  prevEvent: ScenarioEvent;
  constructor(prevEvent: ScenarioEvent) {
    this.prevEvent = prevEvent;
  }
  fire(eventName: string): IEventchainBuilder {
    let ev = new ScenarioEventFire(eventName);
    this.prevEvent.setNext(ev);
    return new EventChainBuilder(ev);
  }
  notify(eventName: string): IEventchainBuilder {
    let ev = new ScenarioEventNotify(eventName);
    this.prevEvent.setNext(ev);
    return new EventChainBuilder(ev);
  }
  where(guard: { (options): boolean; }): IEventchainBuilder {
    let ev = new ScenarioEventWhere(guard);
    this.prevEvent.setNext(ev);
    return new EventChainBuilder(ev);
  }
  pointcut(pointcut: { (EventContext): any; }): IEventchainBuilder {
    let ev = new ScenarioEventPointcut(pointcut);
    this.prevEvent.setNext(ev);
    return new EventChainBuilder(ev);
  }
}
