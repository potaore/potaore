import UTIL = require('./utility');

export class IdentifiedObject {
  public id: string;
  public name: string;
  constructor(name: string) {
    this.id = UTIL.Utility.uuid();
    this.name = name;
  }
  equals(another: IdentifiedObject) {
    return this.id === another.id;
  }
  notEquals(another: IdentifiedObject) {
    return this.id !== another.id;
  }
}

export class ReversibleMap {
  map: any;
  reverseMap: any;
  constructor() {
    this.map = {};
    this.reverseMap = {};
  }
  put(key: string, value: string) {
    this.map[key] = value;
    if (!this.reverseMap.hasOwnProperty(value)) {
      this.reverseMap[value] = {};
    }
    this.reverseMap[value][key] = value;
  }
  remove(key: string) {
    let value: string = this.map[key];
    delete this.reverseMap[value][key];
    delete this.map[key];
  }

  getReverse(value: string) {
    if (!this.reverseMap.hasOwnProperty(value)) {
      this.reverseMap[value] = {};
    }
    return this.reverseMap[value];
  }
}

export class IdentifiedObjectContainer {
  glovalMap: any;
  groupMap: ReversibleMap;
  tagMap: any;
  constructor() {
    this.glovalMap = {};
    this.groupMap = new ReversibleMap();
    this.tagMap = {};
  }
  put(object: IdentifiedObject, group: string = '') {
    this.glovalMap[object.id] = object;
    this.groupMap.put(object.id, group);
  }
  get(id: string) { return this.glovalMap[id] }
  changeGroup(object: IdentifiedObject, group: string = '') {
    this.groupMap.remove(object.id);
    this.groupMap.put(object.id, group);
  }
  remove(object: IdentifiedObject) {
    this.groupMap.remove(object.id);
    delete this.glovalMap[object.id];
  }

  getGroup(group: string) {
    return this.groupMap.getReverse(group);
  }

}
